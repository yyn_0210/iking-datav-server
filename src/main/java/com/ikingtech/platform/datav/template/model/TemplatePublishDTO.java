package com.ikingtech.platform.datav.template.model;

import com.ikingtech.platform.datav.info.model.InfoDTO;
import lombok.Data;

@Data
public class TemplatePublishDTO {

    /**
     * 屏幕信息
     */
    private InfoDTO screen;

    /**
     * 模板信息
     */
    private TemplateDTO template;
}
