package com.ikingtech.platform.datav.component.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.component.entity.TemplateComponent;

public interface TemplateComponentMapper extends BaseMapper<TemplateComponent> {
}