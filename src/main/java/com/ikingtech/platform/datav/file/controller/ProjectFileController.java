package com.ikingtech.platform.datav.file.controller;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.dict.model.DictItemDTO;
import com.ikingtech.framework.sdk.oss.model.OssFileDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.config.FileConstant;
import com.ikingtech.platform.datav.file.entity.ProjectFileDO;
import com.ikingtech.platform.datav.file.exception.ProjectFileExceptionInfo;
import com.ikingtech.platform.datav.file.model.*;
import com.ikingtech.platform.datav.file.service.ProjectFileService;
import com.ikingtech.platform.service.oss.controller.OssFileController;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author fucb
 */
@RestController
@RequiredArgsConstructor
@ApiController(value = "/files", name = "可视化大屏-文件管理", description = "可视化大屏-文件管理")
public class ProjectFileController {

    private final ProjectFileService projectFileService;

    private final OssFileController ossApi;

    @PostRequest(value = "/import/resource", summary = "导入资源包", description = "导入资源包")
    public R<String> importResource(@RequestPart MultipartFile file) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(file.getInputStream(), Charset.forName("gbk"));
        ZipEntry nextEntry;
        int bytesRead;
        byte[] buf = new byte[1024];
        while ((nextEntry = zipInputStream.getNextEntry()) != null) {
            if (!nextEntry.isDirectory()) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                while ((bytesRead = zipInputStream.read(buf, 0, 1024)) > -1) {
                    outputStream.write(buf, 0, bytesRead);
                }

                // 解析并上传文件
                analysisUpload(nextEntry.getName(), outputStream.toByteArray());

                outputStream.close();
            }
            zipInputStream.closeEntry();
        }
        zipInputStream.close();
        return R.ok();
    }

    public void analysisUpload(String entryName, byte[] byteArray) {
        String[] namePath = entryName.split("/");
        String fileName = namePath[namePath.length - 1];

        for (ProjectFileTypeEnum value : ProjectFileTypeEnum.values()) {
            if (entryName.startsWith(value.getValue())) {
                R<OssFileDTO> oss = ossApi.uploadByte(fileName, FileConstant.FILE_RESOURCE_PATH + value.getPath(), byteArray, true);
                if (oss.isSuccess()) {
                    OssFileDTO data = oss.getData();
                    projectFileService.upload(data, value, "");
                }
            }
        }
    }

    @GetRequest(value = "/type", summary = "获取类型", description = "获取类型")
    public R<List<DictItemDTO>> type() {
        List<DictItemDTO> collect = Arrays.stream(ProjectFileTypeEnum.values()).map(x -> {
            DictItemDTO item = new DictItemDTO();
            item.setId(x.name());
            item.setLabel(x.getValue());
            return item;
        }).collect(Collectors.toList());
        return R.ok(collect);
    }

    @PostRequest(value = "upload", summary = "文件上传", description = "文件上传")
    public R<ProjectFileDTO> upload(@RequestPart MultipartFile file,
                                    @RequestParam(required = false) ProjectFileTypeEnum type,
                                    @RequestParam(required = false) String projectId) {
        R<OssFileDTO> oss = ossApi.upload(file, FileConstant.FILE_UPLOAD_PATH, true);
        if (oss.isSuccess()) {
            OssFileDTO data = oss.getData();
            return R.ok(projectFileService.upload(data, type, projectId));
        }
        throw new FrameworkException(ProjectFileExceptionInfo.FILE_UPLOAD_FAILED);
    }

    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询")
    public R<List<ProjectFileVO>> selectByPage(@RequestBody ProjectFileSearchDTO param) {
        return R.ok(this.projectFileService.selectByPage(param).convert(this::modelConvert));
    }

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    public R<ProjectFileVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.projectFileService.getInfo(id));
    }


    @PostRequest(value = "/file/deletes", summary = "删除", description = "删除")
    public R<String> delete(@RequestBody ProjectFileDeleteDTO projectFileDeleteDTO) {
        this.projectFileService.removeBatchByIds(projectFileDeleteDTO.getIds());
        return R.ok();
    }

    private ProjectFileVO modelConvert(ProjectFileDO entity) {
        return Tools.Bean.copy(entity, ProjectFileVO.class);
    }

}

