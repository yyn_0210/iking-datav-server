package com.ikingtech.platform.datav.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.template.entity.TemplateDO;

/**
 * @author fucb
 */
public interface TemplateMapper extends BaseMapper<TemplateDO> {

}

