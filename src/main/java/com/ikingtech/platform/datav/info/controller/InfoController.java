package com.ikingtech.platform.datav.info.controller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.info.entity.InfoDO;
import com.ikingtech.platform.datav.info.model.*;
import com.ikingtech.platform.datav.info.service.InfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author fucb
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@ApiController(value = "/screen", name = "可视化大屏-基础信息表", description = "可视化大屏-基础信息表")
public class InfoController {

    private final InfoService infoService;



    @PostRequest(value = "/page", summary = "查询基础信息表-分页查询", description = "查询基础信息表-分页查询")
    public R<List<InfoVO>> selectByPage(@RequestBody InfoSearchDTO param) {
        return R.ok(this.infoService.selectByPage(param));
    }

    @GetRequest(value = "/get/info", summary = "查询基础信息表详情", description = "查询基础信息表详情")
    public R<InfoVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.infoService.getInfo(id));
    }

    @PostRequest(value = "/add", summary = "新增基础信息表", description = "新增基础信息表")
    public R<InfoVO> add(@RequestBody InfoDTO param) {
        InfoDO info = this.infoService.add(param);
        return R.ok(Tools.Bean.copy(info, InfoVO.class));
    }

    @PostRequest(value = "/update", summary = "编辑基础信息表", description = "编辑基础信息表")
    public R<String> edit(@RequestBody InfoDTO param) {
        this.infoService.edit(param);
        return R.ok();
    }

    @PostRequest(value = "/share", summary = "发布分享大屏", description = "发布分享大屏")
    public R<String> share(@RequestBody InfoDTO param) {
        this.infoService.share(param);
        return R.ok();
    }

    @PostRequest(value = "/delete", summary = "删除基础信息表", description = "删除基础信息表")
    public R<String> delete(@RequestBody InfoDeleteDTO infoDeleteDTO) {
        this.infoService.delete(infoDeleteDTO.getIds());
        return R.ok();
    }

    @PostRequest(value = "/template", summary = "使用模板创建", description = "使用模板创建")
    public R<InfoVO> template(@RequestBody InfoDTO param) {

        InfoDO infoDO = infoService.create(param);

        return R.ok(Tools.Bean.copy(infoDO, InfoVO.class));
    }

    @PostRequest(value = "/rename", summary = "重命名", description = "重命名")
    public R<String> rename(@RequestBody InfoDTO param) {
        InfoDO byId = this.infoService.getById(param.getId());
        byId.setName(param.getName());
        this.infoService.updateById(byId);
        return R.ok();
    }

    @PostRequest(value = "/copy", summary = "复制", description = "复制")
    public R<String> copy(@RequestBody InfoDTO param) {
        infoService.copy(param);
        return R.ok();
    }

    @PostRequest(value = "/publish/info", summary = "获取发布信息", description = "获取发布信息")
    public R<String> exportScreens(@RequestBody InfoDTO param) {
        return R.ok();
    }

}

