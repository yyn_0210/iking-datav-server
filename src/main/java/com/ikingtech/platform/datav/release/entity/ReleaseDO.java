package com.ikingtech.platform.datav.release.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 构建记录表(ReleaseDO)实体类
 *
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_release")
public class ReleaseDO extends BaseEntity {

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 文件URL
     */
    @TableField("file_url")
    private String fileUrl;

    /**
     * 超时时间
     */
    @TableField("time_out")
    private Date timeOut;

    @TableField("version")
    private Integer version;

    /**
     * 项目ID
     */
    @TableField("project_id")
    private String projectId;

    /**
     * 应用ID
     */
    @TableField("screen_id")
    private String screenId;

}

