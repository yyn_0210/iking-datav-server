package com.ikingtech.platform.datav.component.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.component.entity.ComponentDO;

/**
 * 组件表(ComponentDO)数据库访问层
 *
 * @author fucb
 * @since 2024-02-22 09:28:38
 */
public interface ComponentMapper extends BaseMapper<ComponentDO> {

}

