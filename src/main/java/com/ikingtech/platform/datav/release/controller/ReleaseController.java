package com.ikingtech.platform.datav.release.controller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.release.entity.ReleaseDO;
import com.ikingtech.platform.datav.release.pojo.ReleaseDTO;
import com.ikingtech.platform.datav.release.pojo.ReleaseSearchDTO;
import com.ikingtech.platform.datav.release.pojo.ReleaseVO;
import com.ikingtech.platform.datav.release.service.ReleaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author fucb
 */
@RestController
@RequiredArgsConstructor
@ApiController(value = "/release", name = "可视化大屏-构建记录表", description = "可视化大屏-构建记录表")
public class ReleaseController {

    private final ReleaseService releaseService;

    @PostRequest(value = "/select/page", summary = "查询构建记录表-分页查询", description = "查询构建记录表-分页查询")
    public R<List<ReleaseVO>> selectByPage(@RequestBody ReleaseSearchDTO param) {
        return R.ok(this.releaseService.selectByPage(param).convert(this::modelConvert));
    }

    @GetRequest(value = "/get/info", summary = "查询构建记录表详情", description = "查询构建记录表详情")
    public R<ReleaseVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.releaseService.getInfo(id));
    }

    @PostRequest(value = "/add", summary = "新增构建记录表", description = "新增构建记录表")
    public R<String> add(@RequestBody ReleaseDTO param) {
        this.releaseService.add(param);
        return R.ok();
    }

    @PostRequest(value = "/edit", summary = "编辑构建记录表", description = "编辑构建记录表")
    public R<String> edit(@RequestBody ReleaseDTO param) {
        this.releaseService.edit(param);
        return R.ok();
    }

    @DeleteRequest(value = "/delete", summary = "删除构建记录表", description = "删除构建记录表")
    public R<String> delete(@RequestParam("id") String id) {
        this.releaseService.delete(id);
        return R.ok();
    }

    private ReleaseVO modelConvert(ReleaseDO entity) {
        return Tools.Bean.copy(entity, ReleaseVO.class);
    }
}

