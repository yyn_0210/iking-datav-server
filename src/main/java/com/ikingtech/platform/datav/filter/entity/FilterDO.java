package com.ikingtech.platform.datav.filter.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 过滤器表(FilterDO)实体类
 *
 * @author fucb
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_filter")
public class FilterDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 项目ID
     */
    @TableField("project_id")
    private String projectId;

    /**
     * 屏幕ID
     */
    @TableField("screen_id")
    private String screenId;

    /**
     * 过滤器ID
     */
    @TableField("filter_id")
    private String filterId;

    /**
     * 版本
     */
    @TableField("version")
    private Integer version;

    /**
     * Code
     */
    @TableField("code")
    private String code;

    /**
     * 来源
     */
    @TableField("origin")
    private String origin;

    /**
     * 模板id
     */
    @TableField("template_id")
    private String templateId;

    /**
     * 过滤器类型（普通，模板）
     */
    @TableField("filter_Type")
    private String filterType;
}

