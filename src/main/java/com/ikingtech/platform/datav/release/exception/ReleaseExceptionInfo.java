package com.ikingtech.platform.datav.release.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum ReleaseExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 构建记录不存在
     */
    RELEASE_NOT_FOUND("releaseNotFound"),
    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
