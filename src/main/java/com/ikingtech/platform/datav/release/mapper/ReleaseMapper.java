package com.ikingtech.platform.datav.release.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.release.entity.ReleaseDO;

/**
 * @author fucb
 */
public interface ReleaseMapper extends BaseMapper<ReleaseDO> {

}

