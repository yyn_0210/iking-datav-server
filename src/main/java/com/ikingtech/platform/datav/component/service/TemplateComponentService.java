package com.ikingtech.platform.datav.component.service;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.datav.component.mapper.TemplateComponentMapper;
import com.ikingtech.platform.datav.component.entity.TemplateComponent;

import java.util.List;

@Service
public class TemplateComponentService extends ServiceImpl<TemplateComponentMapper, TemplateComponent> {

    public List<TemplateComponent> getComponent(String templateId) {
        return lambdaQuery().eq(TemplateComponent::getTemplateId, templateId).list();
    }

    public List<TemplateComponent> getComponent(List<String> ids) {
        return lambdaQuery().in(TemplateComponent::getId, ids).list();
    }
}
