package com.ikingtech.platform.datav.info.model;

import lombok.Data;

import java.util.List;

/**
 * created on 2024-03-08 11:48
 *
 * @author wub
 */

@Data
public class BuildDeploymentDTO {

    private List<String> id;

    private String projectId;
}
