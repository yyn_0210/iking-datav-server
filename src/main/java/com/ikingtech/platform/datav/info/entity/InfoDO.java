package com.ikingtech.platform.datav.info.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author fucb
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_info")
public class InfoDO extends BaseEntity {

    /**
     * 大屏ID
     */
    @TableField("screen_id")
    private String screenId;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 组ID
     */
    @TableField("group_id")
    private String groupId;

    /**
     * 模板ID
     */
    @TableField("template_id")
    private String templateId;

    /**
     * 分享地址
     */
    @TableField("share_url")
    private String shareUrl;

    /**
     * 分享密码
     */
    @TableField("share_password")
    private String sharePassword;

    /**
     * 分享过期时间
     */
    @TableField("share_time")
    private Date shareTime;

    /**
     * 是否已发布
     */
    @TableField("shared")
    private Boolean shared;

    /**
     * 宽度
     */
    @TableField("width")
    private Integer width;

    /**
     * 高度
     */
    @TableField("height")
    private Integer height;

    /**
     * 背景图
     */
    @TableField("bgimage")
    private String bgimage;

    /**
     * 背景色
     */
    @TableField("bgcolor")
    private String bgcolor;

    /**
     * 间隔像素
     */
    @TableField("grid")
    private Integer grid;

    /**
     * 缩略图
     */
    @TableField("screenshot")
    private String screenshot;

    /**
     * 使用水印
     */
    @TableField("use_watermark")
    private Boolean useWatermark;

    /**
     * 滤镜
     */
    @TableField("style_filter_params")
    private String styleFilterParams;

    /**
     * 子屏
     */
    @TableField("pages")
    private String pages;

    /**
     * 弹窗
     */
    @TableField("dialogs")
    private String dialogs;

    /**
     * 请求配置
     */
    @TableField("address")
    private String address;

    /**
     * 变量
     */
    @TableField("variables")
    private String variables;

    /**
     * 模板图片地址
     */
    @TableField("thumbnail")
    private String thumbnail;

    /**
     * 缩放方式
     */
    @TableField("zoom_mode")
    private Integer zoomMode;

    /**
     * HOST配置
     */
    @TableField("host")
    private String host;

    /**
     * 全局事件
     */
    @TableField("events")
    private String events;

    /**
     * 默认子屏
     */
    @TableField("default_page")
    private String defaultPage;

    /**
     * 嵌入配置
     */
    @TableField("iframe")
    private String iframe;

}

