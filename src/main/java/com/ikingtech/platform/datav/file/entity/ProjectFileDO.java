package com.ikingtech.platform.datav.file.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import com.ikingtech.platform.datav.file.model.ProjectFileTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 文件表(ProjectFileDO)实体类
 *
 * @author fucb
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_project_file")
public class ProjectFileDO extends BaseEntity {

    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 文件名称
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 项目ID
     */
    @TableField("project_id")
    private String projectId;

    /**
     * 文件类型
     */
    @TableField("type")
    private ProjectFileTypeEnum type;

    /**
     * 文件路径
     */
    @TableField("path")
    private String path;

    /**
     * 文件混淆名称
     */
    @TableField("mix_file_name")
    private String mixFileName;

    /**
     * 文件大小
     */
    @TableField("size")
    private Long size;

}

