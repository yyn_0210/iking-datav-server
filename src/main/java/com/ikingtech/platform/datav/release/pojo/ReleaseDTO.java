package com.ikingtech.platform.datav.release.pojo;

import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ReleaseDTO对象", description = "构建记录表对象")
public class ReleaseDTO extends BaseEntity {

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 用户名
     */
    @Schema(name = "userName", description = "用户名")
    private String userName;

    /**
     * 文件URL
     */
    @Schema(name = "fileUrl", description = "文件URL")
    private String fileUrl;

    /**
     * 超时时间
     */
    @Schema(name = "timeOut", description = "超时时间")
    private Date timeOut;

    @Schema(name = "version", description = "${column.comment}")
    private Integer version;

    /**
     * 项目ID
     */
    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    /**
     * 应用ID
     */
    @Schema(name = "screenId", description = "应用ID")
    private String screenId;

}

